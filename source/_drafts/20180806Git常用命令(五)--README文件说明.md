---
title: Git常用命令记录（五）-- README文件介绍
urlname: git-commands-readme
date: 2018-08-06
categories: 
    - Dev
    - Git
tags: [git, command]  
---

![001-GitLogo](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/gitcommand/001-GitLogo.png)

本文是系列文章《Git常用命令记录》的第五篇，记录README文件的相关内容。

<!-- more -->

## README.md操作

- 添加gif  
> https://github.com/taylortaurus/[xxx仓库名]/blob/master/[gif路径]  

- 例如
> https://github.com/taylortaurus/EpGroupSendMesSys_AT/blob/master/EpGroupSendMesSys_AT/screenshot/screenshot.gif