---
title: Git常用命令记录（四）-- gitignore文件说明
urlname: git-commands-gitignore
date: 2018-08-05
categories: 
    - Dev
    - Git
tags: [git, command]  
---


![001-GitLogo](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/gitcommand/001-GitLogo.png)

本文是系列文章《Git常用命令记录》的第四篇。介绍gitignore文件的相关内容。

<!-- more -->