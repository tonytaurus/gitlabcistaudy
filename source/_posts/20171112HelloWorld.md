---
title: Hello World
urlname: hello-world
date: 2017-11-12
categories: 
    - Blog
    - Hexo Blog
tags: [hello world, blog]
---

很久之前，就想搭一个个人博客，从CSDN，博客园，公众号，兜兜转转还是决定买个域名，自己搭个博客。正巧碰上双十一，阿里云域名打折，所以就入手了个域名。搜了几篇教程，开始搭建了这个博客。既然都花钱了，那就好好把这个博客做好吧！