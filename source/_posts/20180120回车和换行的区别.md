---
title: 回车和换行的区别
urlname: CR-and-LF
date: 2018-01-20
categories: 
    - Technology
    - Summary
tags: [控制字符, 历史遗留]  
---

## 历史遗留问题

以下内容摘自[阮一封的网络日志][1]。

>   在计算机还没有出现之前，有一种叫做电传打字机（Teletype Model 33）的玩意，每秒钟可以打10个字符。但是它有一个问题，就是打完一行换行的时候，要用去0.2秒，正好可以打两个字符。要是在这0.2秒里面，又有新的字符传过来，那么这个字符将丢失。
  于是，研制人员想了个办法解决这个问题，就是在每行后面加两个表示结束的字符。一个叫做“回车”，告诉打字机把打印头定位在左边界；另一个叫做“换行”，告诉打字机把纸向下移一行。
  这就是“换行”和“回车”的来历，从它们的英语名字上也可以看出一二。

<!--more-->

## 维基上的定义

**回车**：(英语：Carriage Return(CR))  

> 输入键，又称回车键，控制字符：\r，是指电脑键盘上的Enter键（在Apple键盘上即是Return键），其位置在引号键的右边，另一个位置在数字键盘的右下角。回车码是用以代表换行的控制码之一。  

**换行**：(英语：Line Feed(LF))  

> 在计算机领域中是一种加在文字最后位置的特殊字元：\n，在换行字元的下一个字元将会出现在下一行，实际上换行字元根据不同的硬件平台或操作系统平台会有不同的编码方式。  

## 操作系统间的差异

|Read|LF|CRLF|
|:---|:---|:---|
|“r”:text|LF|LF|
|“rb”:binary|LF|CRLF|
||||
|**Write**|**LF**|**CRLF**|
|“w”:text|CRLF|CRLF|
|“wb”:binary|LF|CRLF|  

## Linux & Mac OS

因为Linux和Mac OS平台与C语言对待EOL的方式完全一致，所以Text Mode和Binary Mode在这些平台下没有任何区别

**参考资料**
1. [维基百科-换行][2]
2. [维基百科-回车][3]
3. [.阮一峰的网络日志-回车和换行][4]
4. [Web开发者-回车换行的故事][5]






[1]: http://www.ruanyifeng.com/blog/2006/04/post_213.html
[2]: https://zh.wikipedia.org/wiki/%E6%8F%9B%E8%A1%8C
[3]: https://zh.wikipedia.org/wiki/%E5%9B%9E%E8%BB%8A%E9%8D%B5
[4]: http://www.ruanyifeng.com/blog/2006/04/post_213.html
[5]: http://www.admin10000.com/document/5685.html