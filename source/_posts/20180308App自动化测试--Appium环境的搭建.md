---
title: App自动化测试--Appium环境的搭建
urlname: app-automation-test-environment
date: 2018-03-08
categories: Automation Test
tags: [automation test, appium, development environment, python, android sdk, jdk]  
description: 本文将详细介绍Windows下，Appium自动化测试环境搭建的步骤。
---

## 所需工具

### 工具说明  

 - **Appium:**  
 现有两个版本，一个是AppiumForWindows_1_4_16_1XX.zip，现已停止维护，但是仍然可用。另外一个是appium-desktop-setup-1.4.0.exe，官方仍在维护的版本。

 官方下载地址：
 [AppiumForWindows版][1]
 [Appium-Desktop版][2]  

 - **Node.js:**  
 根据系统版本下载。
 官方下载地址：[Node][3]  

 - **Android SDK:**  
 由于众所周知的原因，官方访问不了,提供国内下载地址，找到SDK Tools，选择对应版本。
 官方下载地址：[Android SDK][4] 
 国内下载地址：[Android Dev Tools][5]  

 ![001-SDKTools](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/001-SDKTools.png)  

 - **Java:**
 官方下载地址：[Java][6]  

 ![002-JavaDownload](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/002-JavaDownload.png)  

 - **Python:**
 官方下载地址：[Python][7]  
 建议下载3.x版本，因为3.x和2.x不兼容，且语法有出入。  
   ![003-PythonDownload](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/003-PythonDownload.png)  
    
 - **基础组件**  

   - DotNet4.5
   - VC++运行库  
 
 - **工具总预览图**  
 ![004-ToolsOverview](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/004-ToolsOverview.png)  

 提供百度云下载：[AppiumTool][8]，密码：***0517***

---

## 环境搭建步骤  

 - 在环境搭建之前，需要先安装必要的DotNet框架和VC++运行库，自己可以网上下载，博主自己直接安装各个版本的Ranorex Studio，Ranorex Studio安装前会自动检测，并安装缺少的组件。

### Java环境配置

#### 安装Java  

 - 选择Java安装包，如果不修改盘符和路径，一直下一步即可，如果修改盘符和路径，建议把jdk和jre安装在一个文件夹下，如下图。  

 ![005-JavaInstallPath](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/005-JavaInstallPath.png)  
 ![006-JavaFolder](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/006-JavaFolder.png)  

#### 配置Java环境变量  
 
 1. 打开我的电脑 –> 属性 –> 高级 –> 环境变量
 2. 新建系统变量：JAVA_HOME，变量值：你的jdk安装路径，例如:  
  
 ```java
 E:\ProgramFiles\Java\jdk1.8.0_161  
 ```    

 3. 新建系统变量：CLASSPATH，变量值  

 ```java
 .;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar;  
 ```  

   注意“.”和“;”  

 ![007-JavaConfigPath01](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/007-JavaConfigPath01.png)  
 
 4. 修改Path变量值，在末尾添加  

 ```java
 ;%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin;
 ```  
 ![007-JavaConfigPath02](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/007-JavaConfigPath02.png)  
    
 5. 修改好后，可以在终端用java –version和java c命令查看  
 
 ![008-JavaVersion](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/008-JavaVersion.png)  

### Python环境配置  
 
 1. 选择Python安装包，勾选添加到Path中，如下图。  

 ![009-PythonInstallPath](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/009-PythonInstallPath.png)  
 
 2. 安装完成后，打开终端，输入Python，显示版本，如下图。  

 ![010-PythonVersion](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/010-PythonVersion.png)  

### Node环境配置  
 
 1. 默认安装，或是修改盘符安装。  
 2. 安装完成后，打开终端，输入npm，显示如下图。  
 
![011-NodeCommand](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/011-NodeCommand.png)  

### Android SDK环境配置  

#### SDK安装  

 - 直接解压android-sdk_xxxx.zip到你指定目录，如下图。点击SDK Manager.exe，下载你所需要的版本的SDK。  
 
 ![012-SDKFolder](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/012-SDKFolder.png)  

 ![013-UpdateSDK01](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/013-UpdateSDK01.png)  

 ![014-UpdateSDK02](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/014-UpdateSDK02.png)  

#### SDK配置

 1. 新建系统变量：ANDROID_HOME，填写变量值：你的SDK目录，如下图。

 ![015-SDKConfig](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/015-SDKConfig.png)  

 2. 修改Path变量：在路径中添加：  

 ```java
 ;%ANDROID_HOME%\platform-tools;%ANDROID_HOME%\tools;  
 ```  
 3. 在终端输入：adb help，如下图为配置成功。  

 ![016-SDKCommand](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/016-SDKCommand.png)  


### Appium环境配置  

 1. 可以使用命令安装，不推荐，貌似装不了了。  

 ![017-AppiumInstallCommand](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/017-AppiumInstallCommand.png)  

 2. 直接选择安装包安装，两个都可以。  
 3. 在系统变量Path中添加，具体路径根据自己Appium的安装路径而定。  
 4. 在终端输入appium-doctor，如下图显示，即为Appium环境配置成功。  
 
 ![018-AppiumCommand](https://blogimg-1256436395.cos.ap-shanghai.myqcloud.com/appium/018-AppiumCommand.png)  


**参考资料：**

1. [虫师–appium新手入门][9]


[1]: https://bitbucket.org/appium/appium.app/downloads/
[2]: https://github.com/appium/appium-desktop/releases/ 
[3]: https://nodejs.org/en/
[4]: https://developer.android.com/index.html
[5]: http://www.androiddevtools.cn/
[6]: http://www.oracle.com/technetwork/java/javase/downloads/index.html
[7]: https://www.python.org/
[8]: https://pan.baidu.com/s/1yM9isERGsY-vfmQJ15Pagw#list/path=%2F
[9]: http://www.cnblogs.com/fnng/p/4540731.html
