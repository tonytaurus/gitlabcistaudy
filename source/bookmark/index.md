---
title: 书签
date: 2018-05-17 12:27:26
type: bookmark
---


## 收集有趣且有用的站点

|站点类型|站点链接|站点说明|
|:---|:---|:---|
|站长工具类|[Google Search Console][00]|谷歌搜索控制台|
|站长工具类|[百度搜索资源平台][01]|百度站长工具|
|站长工具类|[爱站网][02]|站长工具查询|
||||
|工具类|[TintPNG][10]|免费的在线图片压缩工具|
|工具类|[gitignore][11]|gitignore生成工具|
|工具类|[RGB颜色参考][12]|开源中国的颜色参照表|
|工具类|[Instant][13]|文件转磁力工具|
|工具类|[hacknical][14]|基于Github信息的在线简历生产器|
|工具类|[icon][15]|制作Icon图标|
|工具类|[aconvert][16]|在线转换各种文件格式|
|工具类|[img.shield][17]|shield生成工具|
||||
|浪里个浪|[emoji][20]|好多好多Emoji呀~|





[00]: https://www.google.com/webmasters/tools/home?hl=zh-CN
[01]: https://ziyuan.baidu.com/linksubmit/index
[02]: https://www.aizhan.com/
[10]: https://tinypng.com/
[11]: https://www.gitignore.io/
[12]: http://tool.oschina.net/commons?type=3
[13]: https://instant.io/
[14]: https://hacknical.com/
[15]: http://www.bitbug.net/
[16]: https://www.aconvert.com/
[17]: http://tool.lu/shield/
[20]: http://emoji.muan.co/
